import java.util.HashMap;
import static javax.swing.JOptionPane.*;

public class PropertyRegister {

    // Oppretter et hashmap fordi jeg vil at en eiendom skal hentes ut med en unik ID.

    private HashMap <String, Property> pr = new HashMap <String, Property>();

    public PropertyRegister() {
    }

    // Metode for å registrere en eiendom.
    public void newProperty() {
        int municipalityNumber = Integer.parseInt(showInputDialog("Municipality number:"));
        String municipalityName = showInputDialog("Municipality name:");
        int lotNumber = Integer.parseInt(showInputDialog("Lot number:"));
        int sectionNumber = Integer.parseInt(showInputDialog("Section number:"));
        String name = showInputDialog("Name:");
        double area = Double.parseDouble(showInputDialog("Area:"));
        String nameOfOwner = showInputDialog("Name of owner:");
        String iD = municipalityNumber + "-" + lotNumber + "/" + sectionNumber;

        pr.put(iD, new Property(municipalityNumber, municipalityName, lotNumber, sectionNumber, name, area, nameOfOwner));
        System.out.println(pr);

    }

    //Metode for å liste opp alle eiendommene
    public void listAllProperties() {
        StringBuilder output = new StringBuilder();
        for (Property property : pr.values()) {
                output.append(property).append("\n------------------------------------------\n");
        }
        if (output.isEmpty()) {
            showMessageDialog(null, "There are no registered properties");
        } else {
            showMessageDialog(null, output);
        }

    }

    // Metode for å fjerne en eiendom.
    public void removeProperty(String propertyID) {
        boolean propertyRemoved = false;

        for (String property : pr.keySet()) {
            if (property.equals(propertyID)) {
                pr.remove(property);
                showMessageDialog(null, "Property with ID" + property + " has been removed");
                propertyRemoved = true;
                break;
            }
        }

        if (!propertyRemoved) {
            showMessageDialog(null, "Property with ID " + propertyID + " not found.");
        }

    }

    // Metode for å returnere antall eiendommer i registeret.
    public void amountOfProperties(){
        showMessageDialog(null, "Amount of properties: " + pr.size());
    }

    // Metode for å finne eiendom baser på ID.

    public void findProperty() {
        int municipalityNumber = Integer.parseInt(showInputDialog("Municipality number:"));
        int lotNumber = Integer.parseInt(showInputDialog("Lot number:"));
        int sectionNumber = Integer.parseInt(showInputDialog("Section number:"));
        String propertyID = municipalityNumber + "-" + lotNumber + "/" + sectionNumber;
        boolean propertyFound = false;

        for (String property : pr.keySet()) {
            if (property.equals(propertyID)) {
                showMessageDialog(null, pr.get(property));
                propertyFound = true;
                break;
            }
        }

        if (!propertyFound) {
            showMessageDialog(null, "Property with ID " + propertyID + " not found.");
        }

    }

    public void averageArea() {
        double totalArea = 0;
        for (Property property : pr.values()) {
            totalArea += property.getArea();
        }
        double averageArea = totalArea / pr.size();
        String formattedAverageArea = String.format("%.2f", averageArea);

        showMessageDialog(null, "Average area of all the properties: " + formattedAverageArea + "m2");
      //  showMessageDialog(null, "Average area of all the properties: " + averageArea + "m2");
    }

    public void findPropertyBasedOnLotNumber(int lotNumber) {
        StringBuilder output = new StringBuilder();
        for (Property property : pr.values()) {
            if (property.getLotNumber() == lotNumber) {
                output.append(property).append("\n------------------------------------------\n");
            }
        }
        if (output.isEmpty()) {
            showMessageDialog(null, "There is no property with lot number: " + lotNumber);
        } else {
            showMessageDialog(null, output);
        }
    }

    public void testData() {
        pr.put(1445 + "-" + 77 + "/" + 631, new Property(1445, "Gloppen", 77, 631, "", 1017.6, "Jens Olsen"));
        pr.put(1445 + "-" + 77 + "/" + 131, new Property(1445, "Gloppen", 77, 131, "Syningom", 661.3, "Nicolay Madsen"));
        pr.put(1445 + "-" + 75 + "/" + 19, new Property(1445, "Gloppen", 75, 19, "Fugletun", 650.6, "Evilyn Jensen"));
        pr.put(1445 + "-" + 74 + "/" + 188, new Property(1445, "Gloppen", 74, 188, "", 1457.2, "Karl Ove Bråten"));
        pr.put(1445 + "-" + 69 + "/" + 47, new Property(1445, "Gloppen", 69, 47, "Høiberg", 1339.4, "Elsa Indregård"));
    }


}
