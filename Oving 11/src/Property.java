public class Property {

    // Oppretter objektvariabler for klassen.
    private final int municipalityNumber;
    private final String municipalityName;
    private final int lotNumber;
    private final int sectionNumber;
    private String name;
    private double area;
    private String nameOfOwner;

    // Enkel konstruktør.
    public Property(int municipalityNumber, String municipalityName, int lotNumber, int sectionNumber, String name, double area, String nameOfOwner) {
        this.municipalityNumber = municipalityNumber;
        this.municipalityName = municipalityName;
        this.lotNumber = lotNumber;
        this.sectionNumber = sectionNumber;
        this.name = name;
        this.area = area;
        this.nameOfOwner = nameOfOwner;
    }

    // Getters and setters for nødvendige variabler.
    public int getMunicipalityNumber() {
        return municipalityNumber;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public int getLotNumber() {
        return lotNumber;
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    public String getName() {
        return name;
    }

    public double getArea() {
        return area;
    }

    public String getNameOfOwner() {
        return nameOfOwner;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setNameOfOwner(String nameOfOwner) {
        this.nameOfOwner = nameOfOwner;
    }

    // Metode for å skrive ut ID.

    public String getID () {
        return municipalityNumber + "-" + lotNumber + "/" + sectionNumber;
    }

    @Override
    public String toString() {
        return "Municipality number: " + municipalityNumber +
                "\nMunicipality name: " + municipalityName +
                "\nLot number: " + lotNumber +
                "\nSection number: " + sectionNumber +
                "\nName: " + name +
                "\nArea: " + area + "m2" +
                "\nName of owner: " + nameOfOwner;
    }
}


