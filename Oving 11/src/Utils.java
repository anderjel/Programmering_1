import static javax.swing.JOptionPane.*;

public class Utils {
    PropertyRegister pr = new PropertyRegister();

    public Utils() {
    }

    public void menu(){
        pr.testData();

        while (true) {
            String choice = showInputDialog(
                    "What do you want to do?\n" +
                            "1. Add property\n" +
                            "2. List all properties\n" +
                            "3. Search property\n" +
                            "4. Calculate average area\n" +
                            "5. Remove property\n" +
                            "6. List all properties based on lot number\n" +
                            "7. Amount of properties registered\n" +
                            "8. Exit");

            switch (choice) {
                case "1":
                    pr.newProperty();
                    break;
                case "2":
                    pr.listAllProperties();
                    break;
                case "3":
                    pr.findProperty();
                    break;
                case "4":
                    pr.averageArea();
                    break;
                case "5":
                    pr.removeProperty(showInputDialog("Property ID (Format: Municipality number-Lot number/Section number) e.g. 1504-54/73"));
                    break;
                case "6":
                    pr.findPropertyBasedOnLotNumber(Integer.parseInt(showInputDialog("Lot number:")));
                    break;
                case "7":
                    pr.amountOfProperties();
                    break;
                case "8":
                    System.exit(0);
                default:
                    showMessageDialog(null, "Please choose and option by entering a digit between 1-8");

            }
        }
    }
}
